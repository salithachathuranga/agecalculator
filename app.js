window.onload = () => {
    document.getElementById('yearList').style.display = 'none';
    document.getElementById('monthList').style.display = 'none';
    document.getElementById('dayList').style.display = 'none';
    loadYears();
    loadMonths();
    loadDays();
};

const loadYears = () => {
    const ulYearList = document.getElementById('yearDropdown');
    for (let i = 2020; i >= 1970; i--) {
        const liYear = document.createElement('li');
        liYear.setAttribute('value', i.toString());
        liYear.appendChild(document.createTextNode(i.toString()));
        ulYearList.appendChild(liYear);
    }
}

const loadMonths = () => {
    const ulMonthList = document.getElementById('monthDropdown');
    for (let i = 1; i <= 12; i++) {
        const liMonth = document.createElement('li');
        liMonth.setAttribute('value', i.toString());
        liMonth.appendChild(document.createTextNode(convertDate(i.toString())));
        ulMonthList.appendChild(liMonth);
    }
}

const loadDays = () => {
    const ulDayList = document.getElementById('dayDropdown');
    for (let i = 1; i <= 31; i++) {
        const liDay = document.createElement('li');
        liDay.setAttribute('value', i.toString());
        liDay.appendChild(document.createTextNode(i.toString()));
        ulDayList.appendChild(liDay);
    }
}

const loadDaysWithMonth = (count) => {
    const ulDayList = document.getElementById('dayDropdown');
    for (let i = 1; i <= count; i++) {
        const liDay = document.createElement('li');
        liDay.setAttribute('value', i.toString());
        liDay.appendChild(document.createTextNode(i.toString()));
        ulDayList.appendChild(liDay);
    }
}

const loadValidatedDaysWithMonth = () => {
    let daysCount;
    document.getElementById('dayDropdown').innerHTML = '';
    const monthNumber = document.getElementById('month').value;
    if (monthNumber === '2') {
        daysCount = 28;
    } else if (monthNumber === '1' || monthNumber === '3' || monthNumber === '5' || monthNumber === '7' || monthNumber === '8' ||
        monthNumber === '10' || monthNumber === '12') {
        daysCount = 31;
    } else {
        daysCount = 30;
    }
    loadDaysWithMonth(daysCount);
}

const validateInputs = () => {
    let validated = false;
    const name = document.getElementById('name').value;
    const year = document.getElementById('year').value;
    const month = document.getElementById('month').value;
    const day = document.getElementById('day').value;
    if (name === '' && year === '' && month === '' && day === '') {
        document.getElementById('allError').innerHTML = 'All Fields are Required!';
    } else if (name === '') {
        document.getElementById('nameError').innerHTML = 'Name is Required!';
        document.getElementById('allError').innerHTML = '';
        document.getElementById('ageStatement').innerHTML = '';
    } else if (year === '') {
        document.getElementById('dateError').innerHTML = 'Year is Required!';
        document.getElementById('allError').innerHTML = '';
        document.getElementById('nameError').innerHTML = '';
        document.getElementById('ageStatement').innerHTML = '';
    } else if (month === '') {
        document.getElementById('dateError').innerHTML = 'Month is Required!';
        document.getElementById('allError').innerHTML = '';
        document.getElementById('nameError').innerHTML = '';
        document.getElementById('ageStatement').innerHTML = '';
    } else if (day === '') {
        document.getElementById('dateError').innerHTML = 'Day is Required!';
        document.getElementById('allError').innerHTML = '';
        document.getElementById('nameError').innerHTML = '';
        document.getElementById('ageStatement').innerHTML = '';
    }  else if (day !== '' && 31 < parseInt(day)) {
        document.getElementById('dateError').innerHTML = 'Day is Invalid!';
        document.getElementById('allError').innerHTML = '';
        document.getElementById('nameError').innerHTML = '';
        document.getElementById('ageStatement').innerHTML = '';
    } else {
        let now = new Date();
        if (/^[ a-zA-Z]+$/.test(name)) {
            document.getElementById('nameError').innerHTML = '';
            let birthday = new Date(year, (month - 1), day);
            if (birthday < now) {
                validated = true;
                document.getElementById('allError').innerHTML = '';
                document.getElementById('dateError').innerHTML = '';
                document.getElementById('nameError').innerHTML = '';
                return validated;
            } else {
                document.getElementById('nameError').innerHTML = '';
                document.getElementById('dateError').innerHTML = 'Date Can Not be a Future One!';
                document.getElementById('ageStatement').innerHTML = '';
            }

        } else {
            document.getElementById('nameError').innerHTML = 'Name Can Not Contain Numbers and Special Chars!';
            document.getElementById('ageStatement').innerHTML = '';
        }
    }
    return validated;
}

const pickYear = () => {
    const yearList = document.getElementById('yearList');
    if (yearList.style.display === 'none') {
        yearList.style.display = '';
    } else {
        yearList.style.display = 'none';
    }
    const ulYear = document.getElementById('yearDropdown');
    ulYear.addEventListener('click', (e) => {
        document.getElementById('year').value = e.target.value;
        if (document.getElementById('year').value !== null || document.getElementById('year').value !== undefined) {
            document.getElementById('yearList').style.display = 'none';
        }
    });
}

const pickMonth = () => {
    const monthList = document.getElementById('monthList');
    if (monthList.style.display === 'none') {
        monthList.style.display = '';
    } else {
        monthList.style.display = 'none';
    }
    const ulMonth = document.getElementById('monthDropdown');
    ulMonth.addEventListener('click', (e) => {
        document.getElementById('month').value = e.target.value;
        if (document.getElementById('month').value !== null || document.getElementById('month').value !== undefined) {
            document.getElementById('monthList').style.display = 'none';
        }
    });
}

const pickDate = () => {
    loadValidatedDaysWithMonth();
    const yearSelected = document.getElementById('year').value;
    const monthSelected = document.getElementById('month').value;
    const leapYear = Math.floor(parseInt(yearSelected));
    if (leapYear % 4 === 0 && monthSelected === '2') {
        document.getElementById('dayDropdown').innerHTML = '';
        loadDaysWithMonth(29);
    }
    const dayList = document.getElementById('dayList');
    if (dayList.style.display === 'none') {
        dayList.style.display = '';
    } else {
        dayList.style.display = 'none';
    }
    const ulDay = document.getElementById('dayDropdown');
    ulDay.addEventListener('click', (e) => {
        document.getElementById('day').value = e.target.value;
        if (document.getElementById('day').value !== null || document.getElementById('day').value !== undefined) {
            document.getElementById('dayList').style.display = 'none';
        }
    });
}

const submit = document.getElementById('submitBtn');
submit.addEventListener('click', () => {
    if (validateInputs()) {
        const monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        const name = document.forms['ageForm']['name'].value = document.getElementById('name').value;
        const year = document.forms['ageForm']['year'].value = document.getElementById('year').value;
        const month = document.forms['ageForm']['month'].value = document.getElementById('month').value;
        const day = document.forms['ageForm']['day'].value = document.getElementById('day').value;

        let current = new Date();
        let currentYear = current.getFullYear();
        let currentMonth = current.getMonth() + 1;
        let currentDay = current.getDate();

        if (day > currentDay) {
            currentMonth = currentMonth = currentMonth - 1;
            currentDay = currentDay + monthDays[month - 1];
        }

        if (day < currentDay) {
            currentYear = currentYear - 1;
            currentMonth = currentMonth + 12;
        }

        let calculatedDate = currentDay - day;
        let calculatedMonth = currentMonth - month;
        let calculatedYear = currentYear - year;

        if (calculatedMonth > 12) {
            calculatedYear = calculatedYear + Math.floor(calculatedMonth / 12);
            calculatedMonth = calculatedMonth - 12;
        }
        document.getElementById('ageStatement').innerHTML = `${name} is ${calculatedYear} Years, ${calculatedMonth} Months and ${calculatedDate} Days Old`;
    }
});

const convertDate = (stringDate) => {
    let monthName;
    switch (stringDate) {
        case '1':
            monthName = 'January';
            break;
        case '2':
            monthName = 'February';
            break;
        case '3':
            monthName = 'March';
            break;
        case '4':
            monthName = 'April';
            break;
        case '5':
            monthName = 'May';
            break;
        case '6':
            monthName = 'June';
            break;
        case '7':
            monthName = 'July';
            break;
        case '8':
            monthName = 'August';
            break;
        case '9':
            monthName = 'September';
            break;
        case '10':
            monthName = 'October';
            break;
        case '11':
            monthName = 'November';
            break;
        case '12':
            monthName = 'December';
            break;
        default:
            monthName = '';
    }
    return monthName;
}
